extends MeshInstance

enum DIRECTION {N, NE, NW}

export (int, 20) var max_depth = 4
export var sidelength : float = 16.0

var player_position = Vector3(0,0,0)
var player_view_direction = Vector3(0,0,-1)

var chunks = []
var offsets = []
var chunk
var chunk_list_mutex = Mutex.new()
signal chunk_finished(direction)
var run_in_threads = true

var threads = []
var semaphore = Semaphore.new()
var mutex = Mutex.new()
var stop_threads = false

var _noise : OpenSimplexNoise

func _unthreaded():
		var c = chunk.new(self, Vector3(sidelength, sidelength, sidelength) * Vector3(0,0,0), sidelength, _noise, max_depth)
		c.rebuild()
		
		chunks.append(c)


func _ready():
	_noise = get_parent().noise
	chunk = load("res://scripts/terrain/Chunk.gd")	
	
	if run_in_threads:	
		threads.append(Thread.new())
		#threads.append(Thread.new())
	
		connect("chunk_finished", self, "add_chunk")
		
		for thread in threads:
			thread.start(self, "build_thread", thread)
		
		gen_world_around_player()
		#add_chunk(Vector3(0,0,0))
	
	else:
		_unthreaded()

func build_thread(thread):
	while true:
		print("Thread %s is waiting..." % [ thread.get_id() ])
		
		semaphore.wait()
		
		mutex.lock()
		var should_exit = stop_threads
		mutex.unlock()
		
		if should_exit:
			print("Stopping thread: %s" % thread.get_id())
			break
			
		mutex.lock()
		var offset = offsets.pop_front()
		mutex.unlock()
		
		print("Creating chunk with offset: %s" % offset)
		
		var c = chunk.new(self, Vector3(sidelength, sidelength, sidelength) * offset, sidelength, _noise, max_depth)
		c.rebuild()
		
		print("Thread %s is finished!" % [ thread.get_id() ])
		#emit_signal("chunk_finished", DIRECTION.N)
		
		chunk_list_mutex.lock()
		chunks.append(c)
		chunk_list_mutex.unlock()
		

func gen_world_around_player():
	for y in range(0,4):	
		add_chunk(Vector3(1,y,0))
		add_chunk(Vector3(2,y,0))
		add_chunk(Vector3(-1,y,0))
		add_chunk(Vector3(-2,y,0))
		
		add_chunk(Vector3(0,y,1))
		add_chunk(Vector3(1,y,1))
		add_chunk(Vector3(2,y,1))
		add_chunk(Vector3(-1,y,1))
		add_chunk(Vector3(-2,y,1))
		
		add_chunk(Vector3(0,y,2))
		add_chunk(Vector3(1,y,2))
		add_chunk(Vector3(2,y,2))	
		add_chunk(Vector3(-1,y,2))
		add_chunk(Vector3(-2,y,2))
		
		add_chunk(Vector3(0,y,-1))
		add_chunk(Vector3(-1,y,-1))
		add_chunk(Vector3(-2,y,-1))		
		add_chunk(Vector3(1,y,-1))
		add_chunk(Vector3(2,y,-1))	
		
		add_chunk(Vector3(0,y,-2))
		add_chunk(Vector3(-1,y,-2))
		add_chunk(Vector3(1,y,-2))


func add_chunk(offset : Vector3):
	if len(chunks) < 10:
		mutex.lock()
		offsets.append(offset)
		print("Added offset: %d/%d/%d" % [offset.x, offset.y, offset.z])
		mutex.unlock()
					
		semaphore.post()


func _exit_tree():
	mutex.lock()
	stop_threads = true
	mutex.unlock()
	
	for i in range(0, len(threads)):
		semaphore.post()
	
	for thread in threads:
		thread.wait_to_finish()


func noise_test(x, y, z):
	if (y < 2.00001 and x > 2.1) or (y < 5.02 and x > 5.5 and z < 0.9):
		return 1.0
	else:
		return -1.0
