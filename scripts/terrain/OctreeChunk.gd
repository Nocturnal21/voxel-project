tool
extends Spatial

var noise: OpenSimplexNoise

export (int, 20) var max_depth = 3
export var sidelength : float = 8.0

var tree: OTree
var thread : Thread
var _thread_finished : bool = false
var _waiting : bool = false

var _meshInstance: MeshInstance
var _sf: SurfaceTool
var _vertices = []
var _colors = []
var _node_counter = 0
var _time_delta = 0.0

func config_world(s: int):
	noise.seed = s
	noise.octaves = 4
	noise.period = 8.0
	noise.persistence = 0.3


func _ready():
	randomize()
	#noise = self.get_parent().noise
	noise = OpenSimplexNoise.new()
	config_world(randi())
	
	rebuild()




