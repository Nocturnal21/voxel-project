tool

extends Spatial

var noise = OpenSimplexNoise.new()
var counter = 0


func _ready():
	config_world(randi())
	


# @param s: seed for noise
# @param n: number of chunks to initialize
func config_world(s: int):
	noise.seed = s
	noise.octaves = 4
	noise.period = 20.0
	noise.persistence = 0.5


## Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	if Input.is_action_pressed("ui_reseed"):
#		config_world(randi())
#		$"Chunks".rebuild()

func _input(event):
	if event.is_action_pressed("ui_reseed"):
		counter += 1
#		config_world(randi())
		$"Chunks".add_chunk(Vector3(0,counter,0))
