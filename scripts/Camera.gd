extends Camera


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var sensitivity = 0.1;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_up"):
		self.translate(Vector3(0,0,-1 * sensitivity))
		
	if Input.is_action_pressed("ui_down"):
		self.translate(Vector3(0,0,1 * sensitivity))
		
	if Input.is_action_pressed("ui_right"):
		self.translate(Vector3(1 * sensitivity,0,0))
		
	if Input.is_action_pressed("ui_left"):
		self.translate(Vector3(-1 * sensitivity, 0, 0))
		
