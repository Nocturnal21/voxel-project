tool

var sidelength = 8.0
var noise : OpenSimplexNoise
var node_counter = 0
var max_depth = 5
var parent : Spatial

var _thread : Thread
signal chunk_construction_finished
var _mutex : Mutex
var _semaphore : Semaphore
var _exit_thread = false

var _mesh_instance : MeshInstance
var _mesh : ArrayMesh
var _tree : OTree
var _position : Vector3

func _init(parent_node: Spatial, pos: Vector3, side_length: float, simplex_noise: OpenSimplexNoise, maxd: int):
	sidelength = side_length
	noise = simplex_noise
	max_depth = maxd
	parent = parent_node
	_position = pos
	
	_mesh_instance = MeshInstance.new()
	parent.add_child(_mesh_instance)
	
	_mutex = Mutex.new()
	_semaphore = Semaphore.new()
	_exit_thread = false
	
	_thread = Thread.new()
	_thread.start(self, "_threaded_chunk_construction")
	_semaphore.post()
	
func _threaded_chunk_construction(userdata):
	while true:
		print("Thread %s is waiting..." % [_thread.get_id()])
		_semaphore.wait()
		
		_mutex.lock()
		var should_exit = _exit_thread
		_mutex.unlock()
		
		if should_exit:
			break
			
		_mutex.lock()
		_tree = _build_tree(0, _position.x, _position.y, _position.z)
		create_mesh()
		print("Thread %s is finished!" % [_thread.get_id()])
		_mutex.unlock()


func rebuild():
	_mutex.lock()
	_mesh_instance.queue_free()
	_mesh_instance = MeshInstance.new()
	parent.add_child(_mesh_instance)
	_mutex.unlock()
	
	_semaphore.post()

func _exit_tree():
	_mutex.lock()
	_exit_thread = true
	_mutex.unlock()
	
	_semaphore.post()
	
	_thread.wait_to_finish()


class OTree:
	
	var lBound : Vector3
	var uBound : Vector3
	var empty: bool
	var is_leaf: bool
	var is_intersecting_surface: bool = false
	
	var density_values = []
	
	var bsw : OTree
	var bse : OTree
	var bne : OTree
	var bnw : OTree
	var tsw : OTree
	var tse : OTree
	var tne : OTree
	var tnw : OTree
	
	func _init(l : Vector3, u : Vector3, il: bool):
		lBound = l
		uBound = u
		is_leaf = il


func calc_density(cube: OTree, noise: OpenSimplexNoise):
	# left bottom front corner of cube
	var l = cube.lBound
	# right top back corner of cube (diagonally opposite of 'i')
	var u = cube.uBound
	
	cube.density_values += [
		noise.get_noise_3dv(l),
		noise.get_noise_3d(u.x, l.y, l.z),
		noise.get_noise_3d(u.x, l.y, u.z),
		noise.get_noise_3d(l.x, l.y, u.z),
		noise.get_noise_3d(l.x, u.y, l.z),
		noise.get_noise_3d(u.x, u.y, l.z),
		noise.get_noise_3dv(u),
		noise.get_noise_3d(l.x, u.y, u.z)
	]
	
	var fsign = sign(cube.density_values[0])
	for value in cube.density_values:
		if fsign != sign(value):
			cube.is_intersecting_surface = true
			cube.empty = false
			break
	
	if not cube.is_intersecting_surface:
		if cube.density_values[0] < 0.0:
			cube.empty = true
		else:
			cube.empty = false


func _build_tree(depth: int, x: float, y: float, z: float):
	var current_sl : float = sidelength * pow(0.5, depth)
	var t = OTree.new(Vector3(x,y,z), Vector3(x + current_sl, y + current_sl, z + current_sl), depth == max_depth)
	node_counter += 1
	calc_density(t, noise)
	
	if t.is_intersecting_surface:
		
		if depth < max_depth:
		# check all corners for their density values
		
		# if corners of the tree intersects the density function's surface -> further refine geometry
			var sub_sl = current_sl * 0.5
			
			t.bsw = _build_tree(depth + 1, x, y, z)
			t.bse = _build_tree(depth + 1, x + sub_sl, y, z)
			t.bne = _build_tree(depth + 1, x + sub_sl, y, z + sub_sl)
			t.bnw = _build_tree(depth + 1, x, y, z + sub_sl)
			t.tsw = _build_tree(depth + 1, x, y + sub_sl, z)
			t.tse = _build_tree(depth + 1, x + sub_sl, y + sub_sl, z)
			t.tne = _build_tree(depth + 1, x + sub_sl, y + sub_sl, z + sub_sl)
			t.tnw = _build_tree(depth + 1, x, y + sub_sl, z + sub_sl)
			
		else:
			t.is_leaf = true
	
	else:
		t.is_leaf = true
		
	return t

func construct(t: OTree):
	
	if t != null:
		if t.is_leaf and t.is_intersecting_surface:
			var a = t.lBound
			var b = t.uBound
			var arr = []
			arr.resize(Mesh.ARRAY_MAX)
			
			var verts = PoolVector3Array()
			var uvs = PoolVector2Array()
			var normals = PoolVector3Array()
			var indices = PoolIntArray()
			var colors = PoolColorArray()
			
			verts.push_back(a)
			verts.push_back(Vector3(b.x, a.y, a.z))
			verts.push_back(Vector3(b.x, b.y, a.z))
			verts.push_back(Vector3(a.x, b.y, a.z))
			verts.push_back(Vector3(a.x, a.y, b.z))
			verts.push_back(Vector3(b.x, a.y, b.z))
			verts.push_back(b)
			verts.push_back(Vector3(a.x, b.y, b.z))
			
			uvs.push_back(Vector2(0.375, 0))
			uvs.push_back(Vector2(0.625, 0))
			uvs.push_back(Vector2(0.625, 0.25))
			uvs.push_back(Vector2(0.375, 0.25))
			uvs.push_back(Vector2(0.375, 0.75))
			uvs.push_back(Vector2(0.625, 0.75))
			uvs.push_back(Vector2(0.625, 0.5))
			uvs.push_back(Vector2(0.375, 0.5))
			
			normals.push_back(Vector3(-0.5,-0.5,0.5))
			normals.push_back(Vector3(0.5, -0.5, 0.5))
			normals.push_back(Vector3(0.5, 0.5, 0.5))
			normals.push_back(Vector3(-0.5, 0.5, 0.5))
			normals.push_back(Vector3(-0.5, -0.5, -0.5))
			normals.push_back(Vector3(0.5, -0.5, -0.5))
			normals.push_back(Vector3(0.5, 0.5, -0.5))
			normals.push_back(Vector3(-0.5, 0.5, -0.5))
			
			indices.append_array(PoolIntArray([
				0,1,2,0,2,3,
				1,6,2,1,5,6,
				0,3,7,0,7,4,
				3,2,7,2,6,7,
				0,4,1,1,4,5,
				4,6,5,4,7,6
			]))
			
			arr[Mesh.ARRAY_VERTEX] = verts
			arr[Mesh.ARRAY_TEX_UV] = uvs
			arr[Mesh.ARRAY_NORMAL] = normals
			arr[Mesh.ARRAY_INDEX] = indices
			
			_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arr)
		else:
			construct(t.bsw)
			construct(t.bse)
			construct(t.bne)
			construct(t.bnw)
			construct(t.tsw)
			construct(t.tse)
			construct(t.tne)
			construct(t.tnw)


func create_mesh():
#	_mesh_instance = MeshInstance.new()
	_mesh = ArrayMesh.new()
	var mat = SpatialMaterial.new()
	mat.vertex_color_use_as_albedo = true
	
	_mesh_instance.mesh = _mesh
#	parent.add_child(_mesh_instance)
	
	construct(_tree)
	
	print(_mesh.get_surface_count())


func hsh(x,y,z):
	return (((x << 8) + y) << 8) + z
